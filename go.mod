module mrf-gitlab.south.rt.ru/ms/ms-ssh-file-manager

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/pkg/sftp v1.12.0 // indirect
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392 // indirect
)
