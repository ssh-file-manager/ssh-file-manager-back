package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

const MASTER_HOST int = 0
const ROOT_ELEMENT string = "ROOT"
const REMOTE_QUEUE_DIR = "./remote_queue/"
const REMOTE_SYNCHRONIZED_DIR = "./remote_synchronized/"
const FILE_PERMISSION = 0700

var configFile string

type RemoteHosts struct {
	Hosts []RemoteHost `json:"remote_hosts"`
}

type RemoteHost struct {
	Host              string `json:"host"`
	Port              string `json:"port"`
	User              string `json:"user"`
	Sshkey            string `json:"sshkey"`
	Remote_dir        string `json:"remote_dir"`
	Python_cmd_ending string `json:"python_cmd_ending"`
}

var hosts RemoteHosts

func readConfig() {
	jsonFile, err := os.Open(configFile)
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(byteValue, &hosts)
	if err != nil {
		log.Fatal(err)
	}
}

func getSshClient(nodeIndex int) (*ssh.Client, error) {
	key, err := ioutil.ReadFile(hosts.Hosts[nodeIndex].Sshkey)
	if err != nil {
		return nil, err
	}

	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		return nil, err
	}

	hostKeyCallback, err := knownhosts.New(filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts"))
	if err != nil {
		return nil, err
	}

	config := &ssh.ClientConfig{
		User: hosts.Hosts[nodeIndex].User,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: hostKeyCallback,
	}

	client, err := ssh.Dial("tcp", hosts.Hosts[nodeIndex].Host+":"+hosts.Hosts[nodeIndex].Port, config)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func SSHCopyFile(srcPath, dstPath string, nodeIndex int) error {
	client, err := getSshClient(nodeIndex)
	if err != nil {
		return err
	}
	defer client.Close()

	// open an SFTP session over an existing ssh connection.
	sftp, err := sftp.NewClient(client)
	if err != nil {
		return err
	}
	defer sftp.Close()

	// Open the source file
	srcFile, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	// Create the destination file
	dstFile, err := sftp.Create(dstPath)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	// write to file
	if _, err := dstFile.ReadFrom(srcFile); err != nil {
		return err
	}
	return nil
}

func initDirectories() {
	if _, err := os.Stat(REMOTE_QUEUE_DIR); os.IsNotExist(err) {
		err := os.Mkdir(REMOTE_QUEUE_DIR, FILE_PERMISSION)
		if err != nil {
			log.Fatal(err)
		}
	}

	if _, err := os.Stat(REMOTE_SYNCHRONIZED_DIR); os.IsNotExist(err) {
		err := os.Mkdir(REMOTE_SYNCHRONIZED_DIR, FILE_PERMISSION)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func readDirectory() ([]byte, error) {
	readDirScript := []string{`import os, json`,
		`def path_to_dict(path):`,
		`  d = {"name": os.path.basename(path)}`,
		`  if os.path.isdir(path):`,
		`    d["type"] = "directory"`,
		`    d["children"] = [path_to_dict(os.path.join(path,x)) for x in os.listdir(path)]`,
		`  else:`,
		`    d["type"] = "file"`,
		`  return d`,
		`print (json.dumps(path_to_dict("` + hosts.Hosts[MASTER_HOST].Remote_dir + `")))`,
	}

	client, err := getSshClient(MASTER_HOST)
	if err != nil {
		return nil, err
	}
	defer client.Close()

	session, err := client.NewSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	execommand := strings.Join(readDirScript, "\n")
	err = session.Run("echo '" + execommand + "' | python")
	if err != nil {
		return nil, err
	}

	return stdoutBuf.Bytes(), nil
}

func testSshConnection() {
	fmt.Println("Testing SSH connection...")

	client, err := getSshClient(MASTER_HOST)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	session, err := client.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	fmt.Println("Test SSH connection is ok")
}

func uploadFile(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
		return
	}

	filename := filepath.Base(file.Filename)
	fmt.Println(filename)
	if err := c.SaveUploadedFile(file, REMOTE_QUEUE_DIR+filename); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		return
	}

	if err := SSHCopyFile(REMOTE_QUEUE_DIR+filename, hosts.Hosts[0].Remote_dir+filename, 0); err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("SSHCopyFile err: %s", err.Error()))
		return
	}

	if err := os.Rename(REMOTE_QUEUE_DIR+filename, REMOTE_SYNCHRONIZED_DIR+filename); err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Move file to synchronized folder err: %s", err.Error()))
		return
	}

	c.String(http.StatusOK, "File uploaded")
}

func init() {
	flag.StringVar(&configFile, "config", "./config.json", "config file")
}

func main() {
	flag.Parse()
	readConfig()
	testSshConnection()
	initDirectories()

	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins: []string{"http://localhost:8080"},
		AllowMethods: []string{"POST", "GET"},
	}))

	router.GET("/readdir", func(c *gin.Context) {
		dirTree, err := readDirectory()
		if err != nil {
			c.String(http.StatusInternalServerError, fmt.Sprintf("readdir err: %s", err.Error()))
			return
		}
		c.Data(http.StatusOK, "application/json", dirTree)
	})

	router.POST("/upload", uploadFile)

	router.Run(":8081")
}
