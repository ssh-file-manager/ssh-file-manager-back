# ssh-file-manager-back

SSH file manager back-end is a tool to manage files on remote machine using SSH. You can delete, download and upload files from [front-end](https://gitlab.com/ssh-file-manager/ssh-fm-front)

Main config for connecting to a remote machine are made in the file config.json.

Example:
```json
{
    "remote_hosts": [
        {
            "host": "127.0.0.1",
            "port": "22",
            "user": "jhon",
            "sshkey": "pathtosshkey",
            "remote_dir": "/path/to/remote/dir",
            "python_cmd_ending": "" // empty="python"; "3"="python3"
        }
    ]
}
```
